var graph = {}

for (const h2a of document.body.querySelectorAll("h2 > a[name^='t']")) {
  const name = h2a.text
  console.log(name)

  const entry = graph[name] = {
    wins: {},
    losses: {}
  }

  const table = h2a.parentElement.nextSibling.nextSibling.tBodies[0]
  for (var i = 1; i + 1 < table.rows.length; i++) {
    const row = table.rows[i]

    switch (row.cells[1].textContent) {
      case 'W': {
        const opponent = row.cells[0].textContent

        let PF = 0; let PA = 0

        if (row.cells[2].textContent !== 'Forfeit') {
          PF = parseInt(row.cells[2].textContent)
          PA = parseInt(row.cells[3].textContent)
        }

        const diff = PF - PA

        if (opponent in entry.wins && entry.wins[opponent].diff >= diff) {
          continue
        }

        entry.wins[opponent] = {
          score: [PF, PA],
          diff: diff
        }

        break
      }
      case 'L': {
        const opponent = row.cells[0].textContent

        let PF = 0; let PA = 0

        if (row.cells[2].textContent !== 'Forfeit') {
          PF = parseInt(row.cells[2].textContent)
          PA = parseInt(row.cells[3].textContent)
        }

        const diff = PA - PF

        if (opponent in entry.losses && entry.losses[opponent].diff >= diff) {
          continue
        }

        entry.losses[opponent] = {
          score: [PA, PF],
          diff: diff
        }

        break
      }
      default:
        continue
    }
  }
}

console.log(graph)
