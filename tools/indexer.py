import os
import json

index = []
for f in reversed(sorted(os.listdir("public/graphs"))):
    with open("public/graphs/" + f, "r") as json_file:
        d = json.load(json_file)
        index.append([f, d["name"]])

payload = "\n".join("<option value='{}'>{}</option>".format(i, j) for i, j in index)

with open("index.template.html", "r") as f:
    to_write = f.read().replace("<!-- REPLACE ME -->", payload)
    with open("public/index.html", "w") as f:
        f.write(to_write)

with open("tree.template.html", "r") as f:
    to_write = f.read().replace("<!-- REPLACE ME -->", payload)
    with open("public/tree.html", "w") as f:
        f.write(to_write)