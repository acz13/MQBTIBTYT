import signal  
import sys  
import asyncio  
import aiohttp  
import json

from bs4 import BeautifulSoup
from sys import argv
from collections import OrderedDict as OD

from concurrent.futures import ThreadPoolExecutor

loop = asyncio.get_event_loop()  
executor = ThreadPoolExecutor()

async def get_soup(session, url):  
    async with session.get(url) as response:
        assert response.status == 200
        return BeautifulSoup((await response.read()).decode("utf-8").strip(), "lxml")

def parse(team):
    """Disgusting function for parsing teams"""
    wins = {}
    for row in team.find_all("table")[1].find_all("tr")[1:]:
        tds = row.find_all("td")
        if tds[0].text == "Win" and tds[1].text != "Forfeit":
            a, b = map(int, tds[1].text.replace("(OT)", "").replace(")", "").replace("(", "").split("–"))
            diff = a - b
            opp = row.find_all("th")[1].text
            if opp not in wins or wins[opp]['diff'] <= diff:
                wins[opp] = {
                    'score': (a, b),
                    'diff': diff,
                    'link': tds[1].a.get("href").rsplit("=")[-1]
                }
    return wins

async def get_results(session, url, place):
    root = await get_soup(session, "https://www.naqt.com" + url)
    wins = await loop.run_in_executor(executor, parse, root.find("section", {"id": "team-results"}))
    results = {"wins": wins, "place": place}
    return (root.find("h2").text, results)

async def get_teams(tournament, index=0):
    async with aiohttp.ClientSession() as session:
        field = await get_soup(session, "https://www.naqt.com/stats/tournament/standings.jsp?tournament_id=" + tournament)   
        table = field.find_all("table", {"class": lambda L: L and L.startswith('data-freeze')})[index]
        tasks = []
        for row in table.find_all("tr"):
            anchor = row.find("a")
            if anchor is not None:
                place = row.find("th").text.strip()
                tasks.append(asyncio.ensure_future(get_results(session, anchor['href'], place)))
        return await asyncio.gather(*tasks), field.select_one("ul.breadcrumbs li:nth-last-child(2)").text

def signal_handler(signal, frame):  
    loop.stop()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

if __name__ == "__main__":
    tour_id = sys.argv[1]
    index = 0
    suffix = ""
    if len(sys.argv) > 2:
        index = int(sys.argv[2])
    if len(sys.argv) > 3:
        suffix = " " + sys.argv[3]

    teams, name = loop.run_until_complete(get_teams(tour_id, index))
    teams.sort(key=lambda x: x[0].lower())
    data = {"graph": OD(teams), "_PREFIX": "https://www.naqt.com/stats/tournament/game.jsp?game_id=", "name": name + suffix}
    with open("naqt-{}-{}.json".format(tour_id, index), "w") as f:
        json.dump(data, f)

loop.stop()
sys.exit(0)