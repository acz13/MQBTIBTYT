import json
import sys

file_name = sys.argv[1]
with open(file_name, "r+") as f:
    tournament = json.load(f)
    for winner, team in tournament["graph"].items():
        for loser in team["wins"]:
            if "losses" not in tournament["graph"][loser]:
                tournament["graph"][loser]["losses"] = {}
            tournament["graph"][loser]["losses"][winner] = tournament["graph"][winner]["wins"][loser]

    f.seek(0)
    json.dump(tournament, f)