var nameCheckBox = document.getElementById("show-names");
var teamName = document.getElementById("team-name");
var betterWorse = document.getElementById("better-worse");
var scaling = Math.sqrt;

function d3ifyTree(parents, byLoss) {
    var d3data = { nodes: [], links: [] };
    if (byLoss) { type = "wins" } else type = "losses";
    for (var team in parents) {
        d3data.nodes.push({ "id": team, "place": +(tournament.vertices[team].place) });
        if (parents[team]) {
            d3data.links.push({ "source": team, "target": parents[team], "value": tournament.vertices[team][type][parents[team]]["diff"] });
        }
    }
    return d3data;
}

var color = d3.scaleOrdinal(d3.schemeCategory10);


var canvas = document.querySelector("canvas"),
    context = canvas.getContext("2d"),
    width = canvas.width,
    height = canvas.height;

var graphLayout, center, graph, maxRank;

context.font = context.font.replace(/\d+px/, "8px");

function dispTree() {
    var team = ts1.value;
    if (!team) return;
    teamName.innerHTML = team;
    graph = d3ifyTree(tournament.pathFind(team, false, true, betterWorse.value === "better"), betterWorse.value === "better");

    if (center) center.fixed = false;

    if (!graphLayout) graphLayout = d3.forceSimulation();

    maxRank = d3.max(graph.nodes, d => d.place);

    graphLayout.nodes(graph.nodes)
        .force("charge", d3.forceManyBody().strength(d => -30))
        .force("link", d3.forceLink(graph.links).id(d => d.id).distance(d => d > 8 ? 1 : 10).strength(1))
        .force("x", d3.forceX())
        .force("y", d3.forceY())
        .on("tick", ticked);

    d3.select(canvas)
        .call(d3.drag()
            .container(canvas)
            .subject(dragsubject)
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended));

    d3.select("#link-strength")
        .on("input", changeLinkStrength);

    d3.select("#charge-strength")
        .on("input", changeChargeStrength);

    d3.select("#scale")
        .on("change", changeScaling);


    center = graph.nodes.find(d => d.id === team);
    center.fixed = true;
    center.fx = 0;
    center.fy = 0;

    graphLayout.alpha(1).restart();
}

function changeLinkStrength() {
    graphLayout.force("link").strength(+this.value);
    graphLayout.alpha(1).restart();
}

function changeChargeStrength() {
    graphLayout.force("charge").strength(d => -(+this.value));
    graphLayout.alpha(1).restart();
}

function changeScaling() {
    for (var i = 0; i < this.length; i++) {
        if (this[i].checked) {
            switch (this[i].value) {
                case "sqrt":
                    scaling = Math.sqrt;
                    break;
                case "lin":
                    scaling = keep;
                    break;
                case "square":
                    scaling = square;
                    break;
            }
        }
    }
    ticked();
}

function ticked() {
    context.clearRect(0, 0, width, height);
    context.save();
    context.translate(width / 2, height / 2);

    context.beginPath();
    graph.links.forEach(drawLink);
    context.strokeStyle = "#aaa";
    context.stroke();

    graph.nodes.forEach(drawNode);

    context.restore();
}

function dragsubject() {
    return graphLayout.find(d3.event.x - width / 2, d3.event.y - height / 2);
}

function dragstarted() {
    if (!d3.event.subject.fixed) {
        if (!d3.event.active) graphLayout.alphaTarget(0.3).restart();
        d3.event.subject.fx = d3.event.subject.x;
        d3.event.subject.fy = d3.event.subject.y;
    }
}

function dragged() {
    if (!d3.event.subject.fixed) {
        d3.event.subject.fx = d3.event.x;
        d3.event.subject.fy = d3.event.y;
    }
}

function dragended() {
    if (!d3.event.subject.fixed) {
        if (!d3.event.active) graphLayout.alphaTarget(0);
        d3.event.subject.fx = null;
        d3.event.subject.fy = null;
    }
}

function drawLink(d) {
    context.moveTo(d.source.x, d.source.y);
    context.lineTo(d.target.x, d.target.y);
}

function drawNode(d) {
    context.beginPath();
    var rank = d.place * 0.8 / maxRank;

    context.moveTo(d.x + scaling(1 - rank) * 8, d.y);
    context.arc(d.x, d.y, scaling(1 - rank) * 8, 0, 2 * Math.PI);

    context.fillStyle = d3.interpolateViridis(1 - rank);
    context.strokeStyle = d3.interpolateViridis(1 - rank);

    context.fill();
    context.stroke();

    if (nameCheckBox.checked) context.fillText(d.id, d.x + 3, d.y + 3);
}

function keep(d) { return d; }
function square(d) { return d * d; }

