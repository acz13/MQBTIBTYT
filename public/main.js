var tournament;
var tourSelect = document.getElementById("tournament-select");
var ts1 = document.getElementById("team-select-1");
var ts2 = document.getElementById("team-select-2");
var h3 = document.getElementById("text");
var results = document.getElementById("results");
var prefix = "";

window.onload = function () {
  if (tourSelect.value) {
    loadTournament(tourSelect.value);
  }
};

function loadTournament (url) {
  while (ts1.childElementCount > 1) {
      ts1.removeChild(ts1.lastChild);
  }
while (ts2.childElementCount > 1) {
      ts2.removeChild(ts2.lastChild);
  }
  var req = new XMLHttpRequest();
  req.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var r = JSON.parse(this.responseText);
      prefix = r._PREFIX;
      for (var team in r.graph) {
        var option = document.createElement("option");
        option.appendChild(document.createTextNode(team));
        option.value = team;

        ts1.appendChild(option);
        ts2.appendChild(option.cloneNode(true));
      }
      tournament = new Graph(r.graph);
    }
  }
  req.open("GET", "graphs/" + url, true);
  req.send();
}

function createPlacement(place) {
  var span = document.createElement("span");
  span.className = "placement";
  span.appendChild(document.createTextNode(place));
  return span;
}

function createTd (_class, first, second) {
  var td = document.createElement("td");
  td.className = _class;
  td.appendChild(first);
  if (second) td.appendChild(second);
  return td;
}


function dispPath () {
  var start = ts1.value;
  var target = ts2.value;
  var previous = start;
  var byScore = document.querySelector('input[name="byScore"]:checked').value;
  var path = tournament.pathFind(start, target, byScore);
  var text;

  results.innerHTML = "";

  if (!path) {
    path = tournament.pathFind(target, start, byScore);
    if (path) {
      h3.innerHTML = "I can't prove that " + start + " is better, but I can prove that " + target + " is:";
      previous = target;
    } else {
      h3.innerHTML = "I can't tell if " + start + " or " + target + " is better.";
    }
  } else {
    h3.innerHTML = "";
  }
  

  for (var i = 0; i < path.length; i++) {
    var team = path[i];
    var data = tournament.vertices[previous]["wins"][team];
    console.log(team);
    var tr = document.createElement("tr");

    tr.appendChild(createTd("team right", createPlacement(tournament.vertices[previous].place), document.createTextNode(previous)));

    var score = document.createElement("a");
    score.target = "_blank"; 
    score.href = prefix + data.link;
    score.appendChild(document.createTextNode(data.score.join(" – ")));
    tr.appendChild(createTd("score", score));
    
    tr.appendChild(createTd("team left", document.createTextNode(team), createPlacement(tournament.vertices[team].place)));
    results.appendChild(tr);

    previous = team;
  }
}