"use strict"

function Queue(init, comparator) {
  this.nodes = init || [];
  this.comparator = comparator;
}

Queue.prototype.push = function (key, priority) {
  this.nodes.push([key, priority]);
  this.nodes.sort(this.comparator);
}

Queue.prototype.pop = function () {
  return this.nodes.shift()[0];
}

function Graph(vertices) {
  this.vertices = vertices;
}

function doubleComparator(a, b) {
  return a[1][0] - b[1][0] || b[1][1] - a[1][1];
}

Graph.prototype.pathFind = function (start, target, byScore, byLoss) {
  var mask = start === target ? "ilovepotatochips" : start;
  var queue = new Queue([
    [mask, 0]
  ], doubleComparator);
  var costs = {};
  var parents = {};
  parents[mask] = null;

  for (var vertex in this.vertices) {
    costs[vertex] = [Infinity, 0];
  }

  costs[mask] = [0, 0];

  while (queue.nodes.length) {
    var current = queue.pop();

    if (current === target) {
      var path = [];
      var parent;

      while (parent = parents[current]) {
        path.push(current);
        current = parent;
      }

      return path.reverse();
    }

    if (!current || costs[current][0] === Infinity) {
      continue;
    }

    var obj = current === mask ? this.vertices[start] : this.vertices[current];
    var neighbors = byLoss ? obj.losses : obj.wins;
    for (var neighbor in neighbors) {
      if (costs[current][0] + 1 < costs[neighbor][0]) {
        costs[neighbor] = [costs[current][0] + 1,
        byScore ? costs[current][1] + neighbors[neighbor].diff : Math.random()
        ];
        parents[neighbor] = current;

        queue.push(neighbor, costs[neighbor]);
      }
    }
  }

  return typeof targets === "undefined" ? parents : false;
}

module.exports = Graph;